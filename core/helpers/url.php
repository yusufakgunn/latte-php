<?php
class url
{    
    /**
     * Segment url
     * @params $is indis number
     */
    public function segment($is = '0')
    {
        $url = explode('/', $_GET['url']);
        return (isset($url[$is])) ? $url[$is] : false;
    }
}