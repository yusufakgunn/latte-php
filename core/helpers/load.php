<?php
class _Load
{
    /**
     * Helper load
     */
    public function helper($helperName)
    {
        if (file_exists($file = H_DIR . "/{$helperName}.php")) {
            require $file;
        }else {
            $this->error('helper');
        }   
    }
}