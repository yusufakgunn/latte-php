<?php
class _Database
{
    protected $host, $db, $user, $pass;
    
    /**
     * Construct function
     */
    public function __construct($host, $db, $user, $pass)
    {
        $this->host = $host;
        $this->db = $db;
        $this->user = $user;
        $this->pass = $pass;
    }
}