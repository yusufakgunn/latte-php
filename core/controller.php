<?php
class _Controller extends _View
{
    public $controllerName, $methodName;

    /**
     * Construct function
     * @params $controllerName
     * @params $methodName
     */
    public function __construct($controllerName, $methodName)
    {
        $this->controllerName = $controllerName;
        if (file_exists($file = C_DIR . "/{$controllerName}.php")) {
            require $file;
            $controller = new $controllerName;
            if(method_exists($controller, $methodName)){
                $controller->$methodName();
            }else{
                $this->error('method');
            }
        }else {
            $this->error('404');
        }
    }

}