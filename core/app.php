<?php
class _App extends _View
{
    public $controllerName, $methodName = '', $defaultController = 'index';

    /**
     * Construct function
     * @params $defaultController
     */
    public function __construct($defaultController)
    {
        $this->defaultController = $defaultController;
        $this->route();
    }

    /**
     * Route function
     */
    public function route()
    {
        if(isset($_GET['url'])){
            $url = $_GET['url'];
            $url = explode('/', $url);
            $this->controllerName = $url[0];
            if(isset($url[1])){
                $this->methodName = $url[1];
            }else{
                $this->methodName = 'index';
            }
        }else{
            echo $this->defaultController;
            $this->controllerName = $this->defaultController; // default controller name
        }
    }

    /**
     * Easy and security echo function 
     * @params $input
     */
    public function _e($input)
    {
        echo $input;
    }

    /**
     * Easy and security print function
     * @params $input
     */
    public function _p($input)
    {
        print_r($input);
    }

    /**
     * Easy and security var_dump function
     * @params $input
     */
    public function _d($input)
    {
        var_dump($input);
    }
}