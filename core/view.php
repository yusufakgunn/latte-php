<?php
class _View
{
    /**
     * Call view function
     */
    public function render($view, array $params = [])
    {
        if(file_exists($file = V_DIR . "/{$view}.php")){
            extract($params);
            ob_start();
            require $file;
            echo ob_get_clean();
        }else{
            $this->error('404');
        }   
    }

    /**
     * Call error page
     */
    public function error($code = NULL)
    {
        // Development mode check and exit or log
        if($code == '404'){
            if(file_exists($file = E_DIR . "/404.php")){
                require $file;
            }else{
                echo '404 page not found !';
            }
        }else if($code == 'helper'){
                echo 'Helper not found ...';
                //exit();
        }else if($code == 'method'){
            // method not found
            echo 'Not found...';
            //exit();
        }
    }

    /**
     * Load helper
     */
    public $url; // Url helper
    public function load($helperName)
    {
        if(file_exists($file = H_DIR . '/' . $helperName . '.php')){
            require $file;
            $this->url = new $helperName;
        }else{
            $this->error('helper');
        }
    }
}