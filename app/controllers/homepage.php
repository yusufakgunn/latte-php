<?php
class Homepage extends _App
{
    public $arrays = ['title' => 'Site Başlığı'];

    public function __construct()
    {
       //$this->render('index', $this->arrays);
       $this->load('url');
    }

    public function index()
    {
        echo 'index page';
    }

    public function read()
    {
        $array = array(
            'segment' => $gelen = $this->url->segment(2),
            'segment2' => $gelen = $this->url->segment(3)
        );
        $array = $this->arrays + $array;
        $this->render('read', $array);
    }
}