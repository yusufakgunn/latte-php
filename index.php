<?php
/**
 * Define,
 * PATH = real directory fixed bugs,
 * CORE_DIR, core files directory fixed bugs
 */
define('PATH', realpath('.'));
define('CORE_DIR', PATH . '/core');
define('APP_DIR', PATH . '/app');
define('C_DIR', APP_DIR . '/controllers'); // Controllers dir
define('V_DIR', APP_DIR . '/views'); // Views dir
define('H_DIR', CORE_DIR . '/helpers'); // Helpers dir
define('E_DIR', CORE_DIR . '/error_pages'); // Error pages dir

/**
 * Require core classes
 */
require CORE_DIR . '/view.php';
require CORE_DIR . '/controller.php';
require CORE_DIR . '/app.php';

$defaultController = 'homepage';

$app = new _App($defaultController);
$controller = new _Controller($app->controllerName, $app->methodName);

// development mode eklenecek
$productMode = 'product'; // or development buna göre yazılım karar verecek